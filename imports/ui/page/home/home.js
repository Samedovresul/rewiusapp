import './home.html'
import { Template }    from 'meteor/templating';
import { ReactiveVar } from 'meteor/reactive-var';
import { FlowRouter } from 'meteor/ostrio:flow-router-extra';
import {Images} from  '../../../api/images/collection'
import {Product} from '../../../api/products/collection'
import { Categories } from '../../../api/catogories/collection';
import {Rewiew} from '../../../api/catogories/collection'


Template.home.onCreated(function helloOnCreated() {
    // this.loading=new ReactiveVar(false)
    this.eachCount = new ReactiveVar(0);
    this.productId = new ReactiveVar(0);
    FlowRouter.watchPathChange()
    this.autorun(()=>{
        this.subscribe("getImages")
        this.subscribe("getPosts")
    })
  });

  

  Template.home.helpers({
  loading(){
    return Template.instance().loading.get()
  },
 
});

Template.home.helpers({
    getPosts(){
      return Product.find()
    },
     getImage(){
      return Images.findOne({_id:this.imgId})?.link()
    },
    loggedIn(){
      return Meteor.userId();
    },
    isfeaturedProduct(){
      return Product.find({featured: 'true' })
    },
    notfeaturedProduct(){
      return Product.find({featured: 'false' })
    },
    
})
