import {Template} from  'meteor/templating'
import { ReactiveVar } from 'meteor/reactive-var';
import {FlowRouter} from 'meteor/ostrio:flow-router-extra';
import {Images} from '../../../api/images/collection'
import {Product} from '../../../api/products/collection'
import './category.html'

Template.category.onCreated(function (){
    this.categoryId = new ReactiveVar(0);

    this.autorun(() =>{
        this.subscribe("getImages")
        this.subscribe("getPosts")

        this.categoryId.set(FlowRouter.getParam('_id'))
        console.log(this.categoryId.get())
    })
})

Template.category.helpers({
    loggedIn(){
        return Meteor.userId();
    },
    getPosts(){
        return Product.find()
      },
    getProductcategorys(){
        return Product.find({
            category: Template.instance().categoryId.get()
        })
    },
    getImage(){
    return Images.findOne({_id:this.imgId})?.link()
    },
    isfeaturedProduct(){
        return Product.find({featured: 'true' })
    },
    notfeaturedProduct(){
        return Product.find({featured: 'false' })
    },
})