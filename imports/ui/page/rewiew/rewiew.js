import './rewiew.html'
import { Template }    from 'meteor/templating';
import { ReactiveVar } from 'meteor/reactive-var';
import {FlowRouter} from 'meteor/ostrio:flow-router-extra';
import {Rewiews} from '../../../api/rewiews/collection'
import {Product} from '../../../api/products/collection'
import {Images} from '../../../api/images/collection'

Template.readrewiews.onCreated(function () {
    this.productId = new ReactiveVar();
    this.productRating = new ReactiveVar(0);
    this.reviewCount = new ReactiveVar(0);
    FlowRouter.watchPathChange()

    this.autorun(() =>{
        this.subscribe('getPosts');
        this.subscribe('getImages');
        this.subscribe('getRewiews')
    })

});

Template.readrewiews.helpers({
    getProductId(){
        Template.instance().productId.set(FlowRouter.getParam('_id'))
        return Product.findOne({
            _id: Template.instance().productId.get()
        })
    },
    getstars1(){

        let product = Product.findOne({
            _id: Template.instance().productId.get()
        });

        if(product?.avaregRating == 1){
            return Product.findOne({})
        }

    },
    getstars2(){

        let product = Product.findOne({
            _id: Template.instance().productId.get()
        });

        if(product?.avaregRating == 2){
            return Product.findOne({})
        }

    },
    getstars3(){

        let product = Product.findOne({
            _id: Template.instance().productId.get()
        });

        if(product?.avaregRating == 3){
            return Product.findOne({})
        }

    },
    getstars4(){

        let product = Product.findOne({
            _id: Template.instance().productId.get()
        });

        if(product?.avaregRating == 4){
            return Product.findOne({})
        }

    },
    getstars5(){

        let product = Product.findOne({
            _id: Template.instance().productId.get()
        });

        if(product?.avaregRating == 5){
            return Product.findOne({})
        }

    },
    getImage(){
        return Images.findOne({_id:this.imgId})?.link()
    },
    getRewiew(){
        return Rewiews.find({
            productid: Template.instance().productId.get()
        })
    },
  });
