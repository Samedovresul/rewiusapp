import {Template} from  'meteor/templating'
import { ReactiveVar } from 'meteor/reactive-var';
import {FlowRouter} from 'meteor/ostrio:flow-router-extra';
import {rewiews} from '../../../api/rewiews/collection'
import {Product} from '../../../api/products/collection'
import './addrewiew.html'
Template.addreview.onCreated(function helloOnCreated() {
    this.productId = new ReactiveVar(0)

    this.autorun(()=>{
        this.subscribe("getRewiews")
        this.subscribe("getPosts")
    })
    

})


Template.addreview.helpers({ 
    getProductId(){
        Template.instance().productId.set(FlowRouter.getParam('_id'))
        return Product.findOne({
            _id: Template.instance().productId.get()
        })
    },
    getRewiews(){
        return rewiews.find()
    },
}); 

Template.addreview.events({ 
    'submit #addrewiewrating': function(event, template) { 
        event.preventDefault();
    //    let target = event.target
       let rating = event.target.rating.value;
       let ratingtext = event.target.rewiewtext.value; 

       const  rewiew = {
           rating: parseInt(rating),
           ratingtext: ratingtext,
           productid: template.productId.get(),
           datatime: new Date()
       }
       console.log(rewiew)
       Meteor.call('AddRewiews', rewiew, (err,res)=>{
        if(err) console.log(err);
        if(res) FlowRouter.go('/');
      })

    } 
}); 

