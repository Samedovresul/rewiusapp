
import './add.html'
import { Template }    from 'meteor/templating';
import { ReactiveVar } from 'meteor/reactive-var';
import {Images} from  '../../../api/images/collection'
import {Product} from '../../../api/products/collection';
import {Categories} from '../../../api/catogories/collection';
import { FlowRouter } from 'meteor/ostrio:flow-router-extra';



Template.addProduct.onCreated(function helloOnCreated() {
    // counter starts at 0
    this.loading=new ReactiveVar(false)

    this.autorun(()=>{
        this.subscribe("getImages")
        this.subscribe("getPosts")
        this.subscribe('getCategories')
    })
  });

  Template.addProduct.helpers({
  loading(){
    return Template.instance().loading.get()
  },
 
});

Template.addProduct.helpers({
    loggedIn(){
        return Meteor.userId();
    },
    getPosts(){
      // return Product.findOne({_id:this._id})
      return Product.findOne()
    },
     getImage(){
      return Images.findOne()?.link()
    },
    getCategories(){
      return Categories.find()
    },
})




Template.addProduct.events({
    // 'click #upload'(event, instance) {
    // },
    'submit #newProduct'(event,instance) {
      event.preventDefault();
      // let etarget = document.getElementById('category');
      let target =  event.target
      let Categoryid = target.productCategory.value
      let isFeutured = target.feature.value
      let name = target.name.value;
      let description = target.description.value;

      let file=document.getElementById('myFile').files[0]
      
      const upload = Images.insert({
        file,
        chunkSize: 'dynamic'
      }, false);
  
      upload.on('start', function () {
        instance.loading.set(this);
      });
  
      upload.on('end', function (error, fileObj) {
        if (error) {
          alert(`Error during upload: ${error}`);
        } else {

          const product = {
            name: name,
            description: description,
            imgId: fileObj._id,
            userId: Meteor.userId(),
            category: Categoryid,
            featured: isFeutured,
            avaregRating: 0,
            count: 0
          }
          Meteor.call('Addproduct', product,(err,res)=>{
            if(err) console.log(err);
            if(res) FlowRouter.go('/');
          })
          // console.log(product)
        }
        instance.loading.set(false);
      });
  
      upload.start()
      
  },
  });
