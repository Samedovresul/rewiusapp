import { Template } from 'meteor/templating';
import { ReactiveVar } from 'meteor/reactive-var';

import {Categories} from '../../api/catogories/collection'

import './aside.html'

Template.aside.onCreated(function () {
    this.autorun(() =>{
        this.subscribe('getCategories')
    });
});

Template.aside.helpers({
    getCategories() {
        return Categories.find({});
    }
});
