import './header.html'
import '../../api/products/collection'

Template.header.helpers({
    loggedIn(){
        return Meteor.userId();
    }
})

Template.header.onCreated(function () {
    this.autorun(() => {
        this.subscribe('getPosts')
    })
})