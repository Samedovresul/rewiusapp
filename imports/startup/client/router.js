import '../../ui/layout/layout'
import '../../ui/page/product/add'
import '../../ui/page/home/home'
import '../../ui/page/addrewiew/addrewiew'
import '../../ui/page/rewiew/rewiew'
import '../../ui/page/category/category'
import '../../api/catogories/collection'
import { FlowRouter } from 'meteor/ostrio:flow-router-extra';

FlowRouter.route('/', {
    action() { 
        BlazeLayout.render("mainLayout", {main: 'home'});
    }
});

FlowRouter.route('/add-product', {
    action() { 
        BlazeLayout.render("mainLayout", {main: 'addProduct'});
    }
});


FlowRouter.route('/category/:_id', {
    action() {
        BlazeLayout.render('mainLayout', {main: 'category'});
    }
});


FlowRouter.route('/addreview/:_id', {
    action() {
        BlazeLayout.render('mainLayout', {main: 'addreview'});
    }
});

FlowRouter.route('/readrewiews/:_id', {
    action() {
        BlazeLayout.render('mainLayout', {main: 'readrewiews'});
    }
});

FlowRouter.route('/home', {
    action() {
        BlazeLayout.render('mainLayout', {main: 'home'});
    }
});



